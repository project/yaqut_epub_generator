<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of YaqutConverter
 *
 * @author mistknight
 */
class YaqutConverter {

    private $chapters = [];
    private $tmp;
    private $temp_epub_dir;
    
    private $identifier, $title, $creator, $language, $contributor, $subject, $cover;
    
    private $path_to_cp, $path_to_zip, $path_to_rm;

    public function __construct($tmp = 'tmp', $path_to_cp = '/bin/cp', $path_to_zip = '/usr/bin/zip', $path_to_rm = '/bin/rm') {
        if (stripos($tmp, '/') === 0) { // absolute path
            $this->tmp = $tmp;
        } else {
            $this->tmp = __DIR__ . '/' . $tmp;
        }
        
        $this->path_to_cp = $path_to_cp;
        $this->path_to_zip = $path_to_zip;
        $this->path_to_rm = $path_to_rm;
    }

    public function addChapter($title, $content) {
        $matches = [];
        preg_match_all('/"data:(image\/[\s\S]*?);base64,([\s\S]*?)"/u', $content, $matches);
        
        $images = [];
        
        if (count($matches) > 0) { // images found!
            $i = 0;
            
            while (isset($matches[0][$i])) {
                $extension = explode('/', $matches[1][$i]);
                $extension = $extension[1];
                $image = base64_decode($matches[2][$i]);
                $content = str_replace($matches[0][$i], '"IMAGE['.$i.']"', $content);
                $images[] = [
                    'ext' => $extension,
                    'image' => $image,
                ];
                $i++;
            }
        }
        
        $this->chapters[] = [
            'title' => $title,
            'content' => $content,
            'images' => $images,
        ];
    }

    public function setMeta($identifier = NULL, $title, $creator, $language, $contributor = NULL, $subject, $cover = NULL) {
        if (is_null($identifier)) {
            $this->identifier = $this->genUUID();
        } else {
            $this->identifier = $identifier;
        }
        
        if (is_null($contributor)) {
            $this->contributor = 'Yaqut EPub Generator';
        } else {
            $this->contributor = $contributor;
        }
        
        $this->title = $title;
        $this->creator = $creator;
        $this->language = $language;
        $this->subject = $subject;
        $this->cover = $cover;
    }

    public function generateEpub() {
        $this->temp_epub_dir = $this->tmp . '/' . uniqid();

        shell_exec(escapeshellcmd($this->path_to_cp) . ' -rpf ' . escapeshellarg(__DIR__ . '/template') . ' ' . escapeshellarg($this->temp_epub_dir));

        
        $simpleXML = simplexml_load_file($this->temp_epub_dir . '/OEBPS/content.opf');
        $toc = simplexml_load_file($this->temp_epub_dir . '/OEBPS/toc.ncx');
        $toc->head->meta[0]->attributes()->content = $this->identifier;
        $toc->docTitle->text = $this->title;
        
        $simpleXML->registerXPathNamespace('dc', 'http://purl.org/dc/elements/1.1/');
        $simpleXML->registerXPathNamespace('opf', 'http://www.idpf.org/2007/opf');
                
        $simpleXML->metadata->xpath('dc:title')[0]->{0} = $this->title;
        $simpleXML->metadata->xpath('dc:identifier')[0]->{0} = $this->identifier;
        $simpleXML->metadata->xpath('dc:creator')[0]->{0} = $this->creator;
        $simpleXML->metadata->xpath('dc:language')[0]->{0} = $this->language;
        $simpleXML->metadata->xpath('dc:contributor')[0]->{0} = $this->contributor;
        $simpleXML->metadata->xpath('dc:subject')[0]->{0} = $this->subject;
        $simpleXML->metadata->xpath('dc:date')[0]->{0} = date('Y-m-d');
        
        if (!is_null($this->cover)) {
            $image = file_get_contents($this->cover['uri']);
            
            $ext = explode('/', $this->cover['filemime']);
            $ext = $ext[1];
            
            $image_name = uniqid().'.'.$ext;
            file_put_contents($this->temp_epub_dir.'/OEBPS/Images/'.$image_name, $image);
            
            $new_meta = $simpleXML->metadata->addChild('meta');
            $new_meta->addAttribute('name', 'cover');
            $new_meta->addAttribute('content', $image_name);
            
            $item = $simpleXML->manifest->addChild('item');
            $item->addAttribute('href', 'Images/'.$image_name);
            $item->addAttribute('id', $image_name);
            $item->addAttribute('media-type', 'image/'.$ext);
        }
        
        foreach ($this->chapters AS $key => $value) {
            foreach ($value['images'] AS $key => $image) {
                $image_name = uniqid().'.'.$image['ext'];
                file_put_contents($this->temp_epub_dir.'/OEBPS/Images/'.$image_name, $image['image']);
                $value['content'] = str_replace('IMAGE['.$key.']', '../Images/'.$image_name, $value['content']);
                $item = $simpleXML->manifest->addChild('item');
                $item->addAttribute('href', 'Images/'.$image_name);
                $item->addAttribute('media-type', 'image/'.$image['ext']);
                // <item href="Images/Love%20Stories%20Category%20Art.jpg" id="Love_Stories_Category_Art.jpg" media-type="image/jpeg" />
            }
            $book = file_get_contents($this->temp_epub_dir.'/OEBPS/Text/chapter.xhtml');
            $book = str_replace('[TITLE]', $value['title'], $book);
            $book = str_replace('[CONTENT]', $value['content'], $book);
            $book = str_replace('[EPUBID]', $this->identifier, $book);
            file_put_contents($this->temp_epub_dir.'/OEBPS/Text/chapter-'.($key + 1).'.xhtml', $book);
            $item = $simpleXML->manifest->addChild('item');
            $item->addAttribute('href', 'Text/chapter-'.($key + 1).'.xhtml');
            $item->addAttribute('id', 'chapter-'.($key + 1));
            $item->addAttribute('media-type', 'application/xhtml+xml');
            $item = $simpleXML->spine->addChild('itemref');
            $item->addAttribute('idref', 'chapter-'.($key + 1));
            $item->addAttribute('linear', 'yes');
            
            $nav_point = $toc->navMap->addChild('navPoint');
            $nav_point->addAttribute('id', 'navPoint-'.($key + 1));
            $nav_point->addAttribute('playOrder', $key + 1);
            
            
            $nav_label = $nav_point->addChild('navLabel');
            $text = $nav_label->addChild('text');
            $text->{0} = $value['title'];
            
            $content = $nav_point->addChild('content');
            $content->addAttribute('src', 'Text/chapter-'.($key + 1).'.xhtml');
        }
        
        unlink($this->temp_epub_dir.'/OEBPS/Text/chapter.xhtml');
        
        $simpleXML->asXML($this->temp_epub_dir . '/OEBPS/content.opf');
        $toc->asXML($this->temp_epub_dir . '/OEBPS/toc.ncx');
        
        shell_exec(escapeshellcmd($this->path_to_zip) . ' -r '. escapeshellarg($this->temp_epub_dir.'.epub').' '.escapeshellarg($this->temp_epub_dir));
        shell_exec(escapeshellcmd($this->path_to_rm) . ' -rf '. escapeshellarg($this->temp_epub_dir));
        
        return $this->temp_epub_dir.'.epub';
    }

    private function genUUID() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

}
