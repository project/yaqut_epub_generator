; yaqut make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[ctools][version] = "1.6"
projects[ctools][subdir] = "contrib"

projects[custom_breadcrumbs][version] = "2.0-beta1"
projects[custom_breadcrumbs][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[entityreference_prepopulate][version] = "1.5"
projects[entityreference_prepopulate][subdir] = "contrib"

projects[ercd][version] = "1.0"
projects[ercd][subdir] = "contrib"

projects[field_validation][version] = "2.4"
projects[field_validation][subdir] = "contrib"

projects[l10n_update][version] = "2.0"
projects[l10n_update][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[honeypot][version] = "1.17"
projects[honeypot][subdir] = "contrib"

projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][patch][] = "https://www.drupal.org/files/issues/wysiwyg-support_for_tinymce_4_x-1968319-125.patch"
projects[wysiwyg][subdir] = "contrib"

projects[draggableviews][version] = "2.1"
projects[draggableviews][subdir] = "contrib"

projects[views][version] = "3.10"
projects[views][subdir] = "contrib"

